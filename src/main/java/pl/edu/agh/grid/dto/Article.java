package pl.edu.agh.grid.dto;

import java.util.Date;

import javax.persistence.Id;

public class Article {

	private String ruby_class;
	private String title;
	@Id
	private String _id;
	private Integer page_id;
	private Integer revision_id;
	private Date timestamp;
	private String text;

	public String getRuby_class() {
		return ruby_class;
	}

	public void setRuby_class(String ruby_class) {
		this.ruby_class = ruby_class;
	}

	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public String get_id() {
		return _id;
	}

	public void set_id(String _id) {
		this._id = _id;
	}

	public Integer getPage_id() {
		return page_id;
	}

	public void setPage_id(Integer page_id) {
		this.page_id = page_id;
	}

	public Integer getRevision_id() {
		return revision_id;
	}

	public void setRevision_id(Integer revision_id) {
		this.revision_id = revision_id;
	}

	public Date getTimestamp() {
		return timestamp;
	}

	public void setTimestamp(Date timestamp) {
		this.timestamp = timestamp;
	}

	public String getText() {
		return text;
	}

	public void setText(String text) {
		this.text = text;
	}

}
