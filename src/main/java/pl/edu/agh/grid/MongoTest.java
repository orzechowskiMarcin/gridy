package pl.edu.agh.grid;

import java.io.File;
import java.io.IOException;

import org.codehaus.jackson.JsonFactory;
import org.codehaus.jackson.JsonParser;
import org.codehaus.jackson.JsonProcessingException;
import org.codehaus.jackson.map.MappingIterator;
import org.codehaus.jackson.map.ObjectMapper;

import pl.edu.agh.grid.dto.Article;

public class MongoTest {
	private static final String file_path = "c:\\Moje dokumenty\\!Studia\\IX semestr\\Gridy\\projekt\\articles.json";

	public static void main(String[] args) throws JsonProcessingException, IOException {
		// MongoClient mongo = new MongoClient("localhost", 27017);
		// DB db = mongo.getDB("grid");
		//
		// DBCollection dbCollection = db.getCollection("articles");
		// JacksonDBCollection<Article, String> coll =
		// JacksonDBCollection.wrap(dbCollection, Article.class, String.class);

		ObjectMapper mapper = new ObjectMapper();
		JsonFactory jsonFactory = new JsonFactory();
		JsonParser jsonParser = jsonFactory.createJsonParser(new File(file_path));
		MappingIterator<Article> readValues = mapper.readValues(jsonParser, Article.class);

		long count = 0;
		while (readValues.hasNext()) {
			readValues.next();
			count++;
			// coll.insert(article);
			// System.out.println(article.getTitle());
		}

		System.out.println(count);
	}
}
